import java.util.Scanner;

public class problem1 {
	
	static Scanner in = new Scanner(System.in);

	public static void main(String args[]){
		while(in.hasNextLine()){
			String line = in.nextLine();
			if(!line.isEmpty()){
				String[] numStr = line.split(" ", 2);
				int a = Integer.parseInt(numStr[0]);
				int b = Integer.parseInt(numStr[1]);
			
				if(a < b)
					System.out.println("<\n");
				else if(a == b)
					System.out.println("=\n");
				else if(a > b)
					System.out.println(">\n");
			}
		}
	}

}
