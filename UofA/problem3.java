import java.util.Scanner;

public class problem3{

	static Scanner in = new Scanner(System.in);

	public static void main(String args[]){
		while(in.hasNextLine()){
			String line = in.nextLine();
			if(!line.isEmpty()){
				String[] times = line.split(" ", 2);
				String[] time1 = times[0].split(":", 2);
				String[] time2 = times[1].split(":", 2);

				int hourDif = Integer.parseInt(time2[0]) - Integer.parseInt(time1[0]);
				int minDif = Integer.parseInt(time2[1]) - Integer.parseInt(time1[1]);
				
				if(minDif < 0){
					hourDif--;
					minDif+=60;
				}
		
				System.out.println(hourDif + " " + ((hourDif < 2)?"hour":"hours") + " and " + minDif + ((minDif < 2)?" minute":" minutes") + ".\n");
			}
		}		
	}
}
