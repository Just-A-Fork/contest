import java.util.Scanner;

public class problem5{

	static Scanner in = new Scanner(System.in);

	public static void main(String args[]){
		while(in.hasNextLine()){
			String line1 = in.nextLine();
			if(!line1.isEmpty()){

				String line2 = in.nextLine();
				String line3 = in.nextLine();
				
				String a = line2 + line3;
				String b = line3 + line2;
				
				if(line1.equals(a) || line1.equals(b))
					System.out.println("YES\n");
				else
					System.out.println("NO\n");
			}
	
		}
	}
}
