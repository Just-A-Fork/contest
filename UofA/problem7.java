import java.util.*;

public class problem7{
	
	static Scanner in = new Scanner(System.in);

	public static void main(String args[]){

		while(in.hasNextLine()){

			String line1 = in.nextLine();
			if(!line1.isEmpty()){
			
				List<Integer> day = new ArrayList<Integer>();
				List<Integer> pen = new ArrayList<Integer>();

				for(int i = 0; i < Integer.parseInt(line1); i++){
					
					String line2 = in.nextLine();
					String[] data = line2.split(" ");
					
					day.add(Integer.parseInt(data[0]));
					pen.add(Integer.parseInt(data[1]));
					
					
				}
				int days = day.get(0);
				int sum = 0;
				for(int i = 1; i < day.size(); i++){
					sum+= days * pen.get(i);
					days+= day.get(i);
				}

				System.out.println(sum + "\n");
			}

		}
	}
}
