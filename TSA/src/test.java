import java.util.Scanner;

class Main{

	static Scanner in = new Scanner(System.in);

	public static void main(String args[]){
		
		while(in.hasNextLine()){

			String line = in.nextLine();
			if(!line.isEmpty()){
				try {
					String SplitLine[] = line.split(" ", 2);

					int a = Integer.parseInt(SplitLine[0]);
					int b = Integer.parseInt(SplitLine[1]);

					if(a < b){
						System.out.println("<");
					}else if(a > b){
						System.out.println(">");
					}else{
						System.out.println("=");
					}
				}catch(Exception ignore){}
			}

		}

	}
}
