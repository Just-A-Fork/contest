import java.util.Scanner;

public class contest2 {

	static Scanner in = new Scanner(System.in);

	public static void main(String args[]){

		while(in.hasNextLine()){

			System.out.print("Enter hexadecimal number: ");
			String line = in.nextLine();
			if (!line.isEmpty()) {

				int number = Integer.parseInt(line.substring(2), 16);

				System.out.println("In decimal: " + number);

			}

		}

	}
}
