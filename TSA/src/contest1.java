import java.util.Scanner;
import java.util.Vector;

public class contest1 {

	static Scanner in = new Scanner(System.in);

	public static void main(String args[]){

		Vector<String> lines = new Vector<>();
		while(in.hasNextLine()){
			lines.add(in.nextLine());
		}

		int diceNum = Integer.parseInt(lines.get(0).substring(16));
		int sum = Integer.parseInt(lines.get(1).substring(5));

		findAllCombos(sum, diceNum);

	}

	public static void findAllCombos(int sum, int diceNum){
		findAllCombos(sum, diceNum, new Vector<>());
	}

	public static void findAllCombos(int sum, int diceNum, Vector<Vector<Integer>> returnVal){


		for (int i = 1; i <= 6; i++) {
			if(diceNum > 0) {
				System.out.println(sum);
				findAllCombos(sum - i, diceNum - 1);

			}else{
				return;
			}
		}
	}
}
