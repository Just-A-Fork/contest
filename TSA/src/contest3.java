import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.util.TreeSet;

public class contest3 {

	static Scanner in = new Scanner(System.in);
	static int[] output = new int[3];
	static double bestPerOunce;

	public static void main(String args[]){

		System.out.print("Number of items: ");

		int itemCount = Integer.parseInt(in.nextLine());
		for (int i = 1; i <= itemCount; i++) {

			System.out.print("Weight" + i + ": ");
			double weight = Double.parseDouble(in.nextLine());

			System.out.print("Cost" + i + ": ");
			double cost = Double.parseDouble(in.nextLine());
			double perOunce = Math.floor((cost/weight)*100d)/100d;

			if(perOunce < bestPerOunce || output[0] == 0){
				output = new int[]{i, 0};
				bestPerOunce = perOunce;

			}else if(perOunce == bestPerOunce){
				output[1] = i;
			}

		}

		//not pretty but it works
		//i know theres a better way but i forgot how
		if(output[1] == 0){
			System.out.println("Answer: item " + output[0] + " is the cheapest at " + (((bestPerOunce*100)%10==0)?bestPerOunce +"0": bestPerOunce) + " per ounce");
		}else {
			System.out.println("Answer: items " + output[0] + " and " + output[1] + " are the cheapest at " + (((bestPerOunce*100)%10==0)?bestPerOunce +"0": bestPerOunce) + " per ounce");
		}

	}

}
